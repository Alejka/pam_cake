#!/bin/usr/env bash

###########################
# name : Oleg
# porpuse : homework (LVM)
# date : 25.11.2020
###########################




#--------Variables-------#

sweetcream="/dev/sde"
cheesecream="/dev/sdf"
cherryjam="/dev/sdg"
sweetcream_one="/dev/sde1"
cheesecream_one="/dev/sdf1"
cherryjam_one="/dev/sdg1"

#-----functions----#
main(){
lsblk # shows the details of the hard drives
	sleep 5 # waits 5 seconds
layer_one #creates partition 1 for sde
layer_two #creates partition 1 for sdf
layer_three #creates partition 1 for sdg
put_together #creates pv and vg
add_cream #renames the vg
extend_cake_size #extends pv and vg
cut_the_cake #creates two equal size lv's
lsblk # shows the details of the hard drives
	sleep 5 # waits 5 seconds
}

layer_one(){
fdisk $sweetcream <<EOF
n
p



t
8e
w
q
EOF
}


layer_two(){
fdisk $cheesecream <<EOF
n
p



t
8e
w
q
EOF
}

layer_three(){
fdisk $cherryjam <<EOF
n
p



t
8e
w
q
EOF
}
	
put_together(){
pvcreate $sweetcream_one $cheesecream_one

vgcreate "cake" $sweetcream_one $cheesecream_one

}

add_cream(){
vgrename cake pams_cake
}

extend_cake_size(){
pvresize  $cherryjam_one
vgextend pams_cake $cherryjam_one
}

cut_the_cake(){
lvcreate -n "slice_1" -l50%FREE "pams_cake"
lvcreate -n "slice_2" -l100%FREE "pams_cake"
}
main
